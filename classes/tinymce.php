<?php

/**
 *  @module         TinyMCE
 *  @version        see info.php of this module
 *  @authors        erpe, Dietrich Roland Pehlke (Aldus)
 *  @copyright      2010-2023 erpe, Dietrich Roland Pehlke (Aldus)
 *  @license        MIT  License
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *  Please note: TINYMCE is distributed under the <a href="https://github.com/tinymce/tinymce/blob/develop/LICENSE.TXT">MIT License</a> 
 *
 *
 */

class tinymce extends LEPTON_abstract 
{
    /**
     * Own instance of the class
     * @var object|instance
     */
    public static $instance;

    public string $selector = "";
    /**
     * Required by LEPTON_abstract - called after "getInstance()"
     */
    public function initialize() 
    {
        // Own initializations here:
    }
	/**
	 *	returns the template name of the current displayed page
	 * 
	 *	@param string	$css_path A path to the editor.css - if there is one. Default is an empty string. Call by reference!
	 *	@return string $tiny_template_dir
	 */
	static public function get_template_name( &$css_path = ""): string
	{
		$database = LEPTON_database::getInstance();	
		$lookup_paths = array(
			'/css/editor.css',
			'/editor.css'
		);
		
		$tiny_template_dir = "none";

		/**
		 *	Looking up for an editor.css file for editor
		 *
		 */
		foreach($lookup_paths as $temp_path) 
		{
			if (file_exists(LEPTON_PATH .'/templates/' .DEFAULT_TEMPLATE .$temp_path ) ) 
			{
				$css_path = $temp_path; // keep in mind, that this one is pass_by_reference
				$tiny_template_dir = DEFAULT_TEMPLATE;
				break;
			}
		}
			
		// check if a editor.css file exists in the specified template directory of current page
		if (isset($_GET["page_id"]) && ((int) $_GET["page_id"] > 0)) 
		{
			$pageid = (int) $_GET["page_id"];
			// obtain template folder of current page from the database
			$query_page = "SELECT `template` FROM `" .TABLE_PREFIX ."pages` WHERE `page_id` = ".$pageid;
			$pagetpl = $database->get_one($query_page);
			
			//	check if a specific template is defined for current page
			if (isset($pagetpl) && ($pagetpl != '')) 
			{	
				//	check if a specify editor.css file is contained in that folder
				foreach($lookup_paths as $temp_path) 
				{
					if (file_exists(LEPTON_PATH.'/templates/'.$pagetpl.$temp_path)) 
					{
						$css_path = $temp_path; // keep in mind, that this one is pass_by_reference
						$tiny_template_dir = $pagetpl;
						break;
					}
				}
			}
		}
		return $tiny_template_dir;
	} // get_template_name()


	/**
     * Initialize editor and create an textarea
     *
     * @param string $name  Name of the textarea.
     * @param string $id    Id of the textarea.
     * @param string $content The content to edit.
     * @param int|string|null $width    The width of the editor, overwritten by wysiwyg-admin.
     * @param int|string|null $height   The height of the editor, overwritten by wysiwyg-admin.
     * @param bool $prompt  Direct output to the client via echo (true) or returnd as HTML-textarea (false)?
     * @return bool|string  Could be a BOOL or STR (textarea-tags).
	 *
	 */
	static public function show_wysiwyg_editor(string $name, string $id, string $content, int|string|null $width=null, int|string|null $height=null, bool $prompt=true): bool|string
	{
		global $id_list;
		$database = LEPTON_database::getInstance();
		
		// Get Twig
		$oTWIG = lib_twig_box::getInstance();
		$oTWIG->registerModule('tinymce');
		

        //  [1.0] get values either from wysiwyg_settings class or from internal settings class
		$bWysiwygClassOk = false;
		if(class_exists('wysiwyg_settings',true))
		{
			// [1.1.0] Geht the instance
			$oSettings = wysiwyg_settings::getInstance();
			
			// [1.1.1] Check for entries for this editor
			if( !empty($oSettings->editor_settings) )
			{
				// [1.1.2] Class exists and there are entries for this editor
				$bWysiwygClassOk = true;
			}
		}
                
        //  [1.2] Class "wysiwyg_settings" doesn't exists or there are no entries for this editor:
		if(false === $bWysiwygClassOk)
		{
			//  [1.2.1] Custom class?
			$oSettings = self::getInstance()->getEditorSettings();
		}
			
		/** ***************************
		 *  1. tinyMCE main script part
		 */

		//	make sure that the script-part for the tinyMCE is only load/generated once 
		if (!defined("tinymce_loaded"))
		{
			define("tinymce_loaded", true);
			$tinymce_url = LEPTON_URL."/modules/tinymce/tinymce";
			
			$temp_css_path = "/editor.css";
			$template_name = self::get_template_name( $temp_css_path );
			
			// If editor.css file exists in default template folder or template folder of current page
			$css_file = '"'.LEPTON_URL .'/templates/' .$template_name .$temp_css_path.'"';
			
			if ( !file_exists (LEPTON_PATH .'/templates/' .$template_name .$temp_css_path) ) 
			{
				$css_file = "''";
			}
			
			// If backend.css file exists in default theme folder overwrite module backend.css
			$backend_css = LEPTON_URL.'/templates/'.DEFAULT_THEME.'/backend/tinymce/backend.css';
			if(!file_exists(LEPTON_PATH.'/templates/'.DEFAULT_THEME.'/backend/tinymce/backend.css')) 
			{
				$backend_css = LEPTON_URL.'/modules/tinymce/css/backend.css';
			}
			 

			//	Include language file, if the file is not found (local) we use an empty string (then default = uk.js
			$lang = strtolower( LANGUAGE );
			$language = (file_exists( LEPTON_PATH."/modules/tinymce/tinymce/langs/". $lang .".js" )) ? $lang	: "uk";

			// define filemanager url and access keys
			$filemanager_url = LEPTON_URL."/modules/lib_r_filemanager";
			$akey = password_hash( LEPTON_GUID, PASSWORD_DEFAULT);
			$akey = str_replace(array('$','/'),'',$akey);
			$akey = substr($akey, -30);	
			$_SESSION['rfkey'] = $akey;
	
			$data = [
				'filemanager_url'=> $filemanager_url,
				'LEPTON_URL'    => LEPTON_URL,
				'ACCESS_KEY'    => $akey,			
				'tinymce_url'   => $tinymce_url,
				'backend_css'   => $backend_css,			
				'selector'      => 'textarea:not(#no_wysiwyg)',
				'language'  => $language,      
				'width'     => $oSettings->getWidth(),
				'height'    => $oSettings->getHeight(),
				'css_file'  => $css_file,
				'toolbar'   => $oSettings->getToolbar(),
				'skin'      => $oSettings->getSkin(),
				'content_css'   => $oSettings->getSkin(), // $content_css,
			];
			
			echo $oTWIG->render( 
				"@tinymce/tinymce.lte",	//  template-filename
				$data			//  template-data
			);
		}
		
		/** ****************
		 *  2. textarea part
		 *
		 */
			
		//	values for the textarea
		$data = [
			'id'		=> $id,
			'name'		=> $name,
			'content'	=> htmlspecialchars_decode( $content ),
			'width'		=> $oSettings->getWidth(),
			'height'	=> $oSettings->getHeight()
		];

		$result = $oTWIG->render(
			'@tinymce/textarea.lte',	// template-filename
			$data			// template-data
		);
		
		if ($prompt == true) {
			echo $result;
			return true;
		}
		return $result;
	}	
}
